package com.clv.smolsaitestapp

import android.graphics.Bitmap

interface AIModule {
    fun setOnReceiveImage(callback: (Bitmap) -> Unit)
    fun setOnError(callback: () -> Unit)
    fun getName(): String
}

interface GANModule: AIModule {
    fun generate(inputBitmap: Bitmap, styleImage: Bitmap?)
}
