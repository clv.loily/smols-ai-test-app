package com.clv.smolsaitestapp

import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.graphics.scale
import com.clv.smolsaitestapp.ui.theme.SmolsAITestAppTheme


class MainActivity : ComponentActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initModules()

        setContent {
            val scaffoldState = rememberScaffoldState(rememberDrawerState(DrawerValue.Closed))
            SmolsAITestAppTheme {
                Scaffold(
                    scaffoldState = scaffoldState,
                    modifier = Modifier.fillMaxSize(),
                    topBar = {
                        TopAppBar(
                            title = { Text("GAN Models") },
                            backgroundColor = MaterialTheme.colors.primary,
                            navigationIcon = {
                                IconButton(onClick = {
                                }) {
                                    Icon(
                                        painterResource(id = R.drawable.ic_baseline_menu_24),
                                        contentDescription = "Menu"
                                    )
                                }
                            }
                        )
                    },
                    content = {
                        GANDemo()
                    }
                )
            }
        }
    }

    private fun initModules() {

    }
}

@Composable
fun LoadImageButton(onSelectBitmap: () -> Unit) {
    Button(onClick = {
        onSelectBitmap()
    }, shape = RoundedCornerShape(50)) {
        Image(
            painter = painterResource(id = R.drawable.ic_baseline_add_a_photo_24),
            contentDescription = null
        )
        Text(text = "Select image", Modifier.padding(start = 10.dp))
    }
}

@Composable
fun RunModuleButton(isEnable: Boolean, onRun: () -> Unit) {
    Button(
        enabled = isEnable,
        onClick = onRun,
        shape = RoundedCornerShape(50)
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_baseline_play_arrow_24),
            contentDescription = null
        )
        Text(text = "Run model", Modifier.padding(start = 10.dp))
    }
}

@Composable
fun InputImage(bitmap: Bitmap) {
    ShowImage(bitmap, "Input Image")
}

@Composable
fun GANDemo() {
    val context = LocalContext.current
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp.dp
    val modules = arrayOf(
        Pair(WesternAPIModule(), 5),
//        WesternTFLiteModule(context, Executors.newSingleThreadScheduledExecutor()),
        Pair(EasternAPIModule(), 1),
//        EasternTFLiteModule(context, Executors.newSingleThreadScheduledExecutor()),
    )
    val expectedNumOfImages = modules.sumOf { it.second }
    val resultImages = remember { mutableStateListOf<Pair<Bitmap?, String>>() }
    var selectedBitmap by remember { mutableStateOf<Bitmap?>(null) }
    var runningState by remember { mutableStateOf("init") }
    val galleryLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) {
            val source = ImageDecoder.createSource(context.contentResolver, it)
            val inputBitmap = ImageDecoder.decodeBitmap(source) { decoder, _, _ ->
                decoder.isMutableRequired = true
            }
            selectedBitmap = inputBitmap
        }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        LoadImageButton { galleryLauncher.launch("image/*") }
        if (selectedBitmap != null) {
            InputImage(bitmap = selectedBitmap!!)
            RunModuleButton(isEnable = runningState == "init") {
                runningState = "running"
                resultImages.clear()
                for ((module, num) in modules) {
                    module.setOnReceiveImage {
                        val scaleFactor = (screenWidth.value / it.width)
                        val targetWidth = screenWidth.value.toInt()
                        val targetHeight = (it.height * scaleFactor).toInt()
                        val resizedImage = it.scale(targetWidth, targetHeight)
                        resultImages.add(Pair(resizedImage, module.getName()))
                    }
                    module.setOnError {
                        resultImages.add(Pair(null, module.getName()))
                    }
                    for (index in 1..num) {
                        module.generate(selectedBitmap!!, null)
                    }
                }
            }
        }
        if (resultImages.size == expectedNumOfImages) {
            runningState = "finished"
        }
        if (runningState == "running") {
            CircularProgressIndicator(modifier = Modifier.width(32.dp))
        } else if (runningState == "finished") {
            val sortedResults =
                resultImages.filter { it.first != null }.map { Pair(it.first!!, it.second) }
                    .sortedBy { it.second }
            ResultImages(sortedResults)
            runningState = "init"
        }
    }
}

@Composable()
fun ResultImages(results: List<Pair<Bitmap, String>>) {
    LazyRow {
        items(results) { item ->
            OutputImage(item.first, item.second)
        }
    }
}

@Composable
fun ShowImage(bitmap: Bitmap, description: String) {
    Box(modifier = Modifier.fillMaxSize()) {
        Image(
            bitmap = bitmap.asImageBitmap(),
            contentDescription = description,
            contentScale = ContentScale.FillWidth,
            alignment = Alignment.TopCenter,
            modifier = Modifier.fillMaxWidth(),
        )
    }
}


@Composable
fun OutputImage(bitmap: Bitmap, caption: String? = null) {
    val configuration = LocalConfiguration.current
    val screenWidth = configuration.screenWidthDp.dp
    Column(
        modifier = Modifier.width(screenWidth),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        ShowImage(bitmap, "Output Image")
        if (caption != null) {
            Text(caption, modifier = Modifier.padding(top = 10.dp))
        }
    }
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    SmolsAITestAppTheme {

    }
}