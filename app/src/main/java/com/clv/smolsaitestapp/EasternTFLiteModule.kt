package com.clv.smolsaitestapp

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.util.Size
import androidx.core.graphics.scale
import org.tensorflow.lite.Interpreter
import java.io.File
import java.io.FileInputStream
import java.lang.RuntimeException
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.util.concurrent.Executor
import java.util.logging.Handler
import kotlin.random.Random

class EasternTFLiteModule(private val context: Context, private val executor: Executor): BaseAIModule(), GANModule {

    private lateinit var decodeModel: Interpreter
    private var isInit = false

    init {
        initModule()
    }

    private fun initModule() {
        if (isInit) {
            return
        }
        val options = TFLiteUtils.makeInterpreterOptions(false, 2)
        val mappedByteBuffer: MappedByteBuffer
        context.assets.openFd("p2phd.tflite").use {
            val inputStream = FileInputStream(it.fileDescriptor)
            val fileChannel = inputStream.channel
            val startOffset = it.startOffset
            val declaredLength = it.declaredLength
            mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
        }
        decodeModel = Interpreter(mappedByteBuffer, options)
        decodeModel.allocateTensors()
        isInit = true
    }

    override fun generate(inputBitmap: Bitmap, styleImage: Bitmap?) {
        executor.execute {
            val outputBitmap = generateImpl(inputBitmap, styleImage)
            onImageCallback?.let {
                it(outputBitmap)
            }
        }
    }

    private fun generateImpl(inputBitmap: Bitmap, styleImage: Bitmap?): Bitmap {
        val resizedImage = inputBitmap.scale(256, 256)
        val inputArray = TFLiteUtils.bitmapToArray(resizedImage, true)
        val normalized = FloatArray(inputArray.size)
        for (i in inputArray.indices) {
            normalized[i] = ((inputArray[i] / 255.0f) - 0.5f) / 0.5f
        }
        val inputBuffer = FloatBuffer.wrap(normalized)

        val outBuffer = ByteBuffer.allocateDirect(1 * 3 * 256 * 256 * Float.SIZE_BYTES)
        outBuffer.order(ByteOrder.nativeOrder())
        decodeModel.run(inputBuffer, outBuffer)
        outBuffer.rewind()

        val outputFloatArray = FloatArray(3 * 256 * 256)
        outBuffer.asFloatBuffer().get(outputFloatArray)

        val outputBitmap = postProcessOutput(inputBitmap, outputFloatArray)
        return outputBitmap
    }

    private fun postProcessOutput(inputBitmap: Bitmap, outputFloatArray: FloatArray): Bitmap {
        val minValue = outputFloatArray.minOrNull()!!
        val maxValue = outputFloatArray.maxOrNull()!!
        val denormalized = FloatArray(outputFloatArray.size)
        for (index in outputFloatArray.indices) {
            denormalized[index] = (outputFloatArray[index] - minValue) / (maxValue - minValue)
        }

        val outputBitmap = TFLiteUtils.arrayToBitmap(denormalized, Size(256, 256), true)
        return outputBitmap.scale(inputBitmap.width, inputBitmap.height)
    }

    override fun getName(): String {
        return "Eastern (Offline)"
    }

}
