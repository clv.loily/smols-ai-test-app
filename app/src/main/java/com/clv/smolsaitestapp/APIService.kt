package com.clv.smolsaitestapp

import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface WesternAPIService  {
    @POST("/western/file")
    @Multipart
    fun uploadImage(@Part file: MultipartBody.Part): Call<ResponseBody>
}

interface EasternAPIService  {
    @POST("/eastern/file")
    @Multipart
    fun uploadImage(@Part file: MultipartBody.Part): Call<ResponseBody>
}
