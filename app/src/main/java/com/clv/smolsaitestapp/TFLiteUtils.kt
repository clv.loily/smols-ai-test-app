package com.clv.smolsaitestapp

import android.content.Context
import android.graphics.Bitmap
import android.util.Size
import java.io.FileInputStream
import java.nio.channels.FileChannel
import org.tensorflow.lite.Interpreter
//import org.tensorflow.lite.gpu.CompatibilityList
//import org.tensorflow.lite.gpu.GpuDelegate
import java.io.File

class TFLiteUtils {

    companion object {

        fun loadInterpreterFromRaw(
            context: Context,
            modelName: String,
            options: Interpreter.Options
        ): Interpreter {
            val fileDescriptor = context.resources.openRawResourceFd(
                context.resources.getIdentifier(modelName, "raw", context.packageName)
            )
            val fileChannel = FileInputStream(fileDescriptor.fileDescriptor).channel
            val modelFile = fileChannel.map(
                FileChannel.MapMode.READ_ONLY,
                fileDescriptor.startOffset,
                fileDescriptor.declaredLength
            )
            return Interpreter(modelFile, options)
        }

        fun loadInterpreterFromPath(
            modelPath: String,
            options: Interpreter.Options
        ): Interpreter {
            val modelFile = File(modelPath)
            return Interpreter(modelFile, options)
        }

        fun loadInterpreterFromFile(
            modelFile: File,
            options: Interpreter.Options
        ): Interpreter {
            return Interpreter(modelFile, options)
        }

        fun makeInterpreterOptions(
            useGPU: Boolean = false,
            numThreads: Int = 4
        ): Interpreter.Options {
//            val compatList = CompatibilityList()
//            return Interpreter.Options().apply {
//                if (useGPU && compatList.isDelegateSupportedOnThisDevice) {
//                    // if the device has a supported GPU, add the GPU delegate
//                    val delegateOptions = compatList.bestOptionsForThisDevice
//                    this.addDelegate(GpuDelegate(delegateOptions))
//                } else {
//                    // if the GPU is not supported, run on 4 threads
//                    this.setNumThreads(numThreads)
//                }
//            }
            return Interpreter.Options().apply {
                this.setNumThreads(numThreads)
            }
        }

        fun isGPUAvailable(): Boolean {
//            val compatList = CompatibilityList()
//            return compatList.isDelegateSupportedOnThisDevice
            return false
        }

        fun encodeColor(a: Int, r: Int, g: Int, b: Int): Int {
            return (a and 0xff shl 24) or (r and 0xff shl 16) or (g and 0xff shl 8) or (b and 0xff)
        }

        fun encodeColor(a: Float, r: Float, g: Float, b: Float): Int {
            return encodeColor(
                (a * 255).toInt(),
                (r * 255).toInt(),
                (g * 255).toInt(),
                (b * 255).toInt()
            )
        }

        fun decodeColor(color: Int): IntArray {
            val a: Int = color shr 24 and 0xff
            val r: Int = color shr 16 and 0xff
            val g: Int = color shr 8 and 0xff
            val b: Int = color and 0xff
            return intArrayOf(a, r, g, b)
        }

        fun bitmapToArray(inputBitmap: Bitmap, isChannelFirst: Boolean = false): FloatArray {
            val outArray = FloatArray(inputBitmap.width * inputBitmap.height * 3)
            val inputArray = IntArray(inputBitmap.width * inputBitmap.height)
            inputBitmap.getPixels(inputArray, 0, inputBitmap.width, 0, 0, inputBitmap.width, inputBitmap.height)

            if (isChannelFirst) {
                for (i in inputArray.indices) {
                    val argb = decodeColor(inputArray[i])
                    outArray[0 * inputBitmap.width * inputBitmap.height + i] = argb[1].toFloat()
                    outArray[1 * inputBitmap.width * inputBitmap.height + i] = argb[2].toFloat()
                    outArray[2 * inputBitmap.width * inputBitmap.height + i] = argb[3].toFloat()
                }
            } else {
                TODO()
            }

            return outArray
        }

        fun arrayToBitmap(
            floatArray: FloatArray,
            imageSize: Size,
            isChannelFirst: Boolean = false
        ): Bitmap {
            val intArray = IntArray(imageSize.width * imageSize.height)
            if (isChannelFirst) {
                for (index in intArray.indices) {
                    val r = floatArray[0 * imageSize.width * imageSize.height + index]
                    val g = floatArray[1 * imageSize.width * imageSize.height + index]
                    val b = floatArray[2 * imageSize.width * imageSize.height + index]
                    val intValue = encodeColor(1.0f, r, g, b)
                    intArray[index] = intValue
                }
            } else {
                TODO()
            }
            return Bitmap.createBitmap(
                intArray,
                imageSize.width,
                imageSize.height,
                Bitmap.Config.ARGB_8888
            )
        }
    }
}