package com.clv.smolsaitestapp

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.util.Size
import androidx.core.graphics.scale
import org.tensorflow.lite.Interpreter
import java.io.FileInputStream
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import java.nio.MappedByteBuffer
import java.nio.channels.FileChannel
import java.util.concurrent.Executor
import kotlin.random.Random

class WesternTFLiteModule(private val context: Context, private val executor: Executor) : BaseAIModule(), GANModule {

    private lateinit var decodeModel: Interpreter
    private var isInit = false

    init {
        initModule()
    }

    private fun initModule() {
        if (isInit) {
            return
        }
        val options = TFLiteUtils.makeInterpreterOptions(false, 2)
        val mappedByteBuffer: MappedByteBuffer
        context.assets.openFd("gannrose_decode.tflite").use {
            val inputStream = FileInputStream(it.fileDescriptor)
            val fileChannel = inputStream.channel
            val startOffset = it.startOffset
            val declaredLength = it.declaredLength
            mappedByteBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength)
        }
        decodeModel = Interpreter(mappedByteBuffer, options)
        decodeModel.allocateTensors()

        isInit = true
    }

    override fun generate(inputBitmap: Bitmap, styleImage: Bitmap?) {
        executor.execute {
            val outputBitmap = generateImpl(inputBitmap, styleImage)
            onImageCallback?.let {
                it(outputBitmap)
            }
        }
    }

    private fun generateImpl(inputBitmap: Bitmap, styleImage: Bitmap?): Bitmap {
        val encodedStyle = encodeStyle(styleImage)
        return decode(inputBitmap, encodedStyle)
    }

    override fun getName(): String {
        return "Western (Offline)"
    }

    private fun encodeStyle(styleImage: Bitmap?): FloatArray {
        if (styleImage == null) {
            val array = FloatArray(8)
            for (i in array.indices) {
                array[i] = Random.nextFloat()
            }
            return array
        } else {
            TODO("Not implemented")
        }
    }

    private fun decode(inputBitmap: Bitmap, encodedStyle: FloatArray): Bitmap {
        val resizedImage = inputBitmap.scale(256, 256)
        val inputArray = TFLiteUtils.bitmapToArray(resizedImage, true)
        val normalized = FloatArray(inputArray.size)
        for (i in inputArray.indices) {
            normalized[i] = ((inputArray[i] / 255.0f) - 0.5f) / 0.5f
        }

        val input0 = FloatBuffer.wrap(normalized)
        val input1 = FloatBuffer.wrap(encodedStyle)
        val inputs = arrayOf(input0, input1)

        val outputPlaceholders = mapOf(0 to ByteBuffer.allocateDirect(3 * 256 * 256 * Float.SIZE_BYTES).apply { this.order(ByteOrder.nativeOrder()) })
        decodeModel.runForMultipleInputsOutputs(inputs, outputPlaceholders)
        outputPlaceholders[0]!!.rewind()

        val outputFloatArray = FloatArray(3 * 256 * 256)
        outputPlaceholders[0]!!.asFloatBuffer().get(outputFloatArray)

        return postProcessOutput(inputBitmap, outputFloatArray)
    }

    private fun postProcessOutput(inputBitmap: Bitmap, outputFloatArray: FloatArray): Bitmap {
        val minValue = outputFloatArray.minOrNull()!!
        val maxValue = outputFloatArray.maxOrNull()!!
        val denormalized = FloatArray(outputFloatArray.size)
        for (index in outputFloatArray.indices) {
            denormalized[index] = (outputFloatArray[index] - minValue) / (maxValue - minValue)
        }

        val outputBitmap = TFLiteUtils.arrayToBitmap(denormalized, Size(256, 256), true)
        return outputBitmap.scale(inputBitmap.width, inputBitmap.height)
    }

}