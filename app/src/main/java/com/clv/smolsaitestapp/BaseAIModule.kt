package com.clv.smolsaitestapp

import android.graphics.Bitmap

abstract class BaseAIModule: AIModule {

    protected var onErrorCallback: (() -> Unit)? = null
    protected var onImageCallback: ((Bitmap) -> Unit)? = null

    override fun setOnError(callback: () -> Unit) {
        onErrorCallback = callback
    }

    override fun setOnReceiveImage(callback: (Bitmap) -> Unit) {
        onImageCallback = callback
    }
}