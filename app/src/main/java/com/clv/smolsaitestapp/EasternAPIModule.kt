package com.clv.smolsaitestapp

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.core.graphics.scale
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.ByteArrayOutputStream


class EasternAPIModule: BaseAIModule(), GANModule {

    companion object {
        private val SERVICE_URL = "http://3.35.5.193:8000/"
    }

    override fun getName(): String {
        return "Eastern"
    }

    override fun generate(inputBitmap: Bitmap, styleImage: Bitmap?) {
        val resizedInputBitmap = inputBitmap.scale(256, 256)
        val outStream = ByteArrayOutputStream()
        resizedInputBitmap.compress(Bitmap.CompressFormat.PNG, 100, outStream)
        val fileBody: RequestBody = RequestBody.create(MediaType.parse("image/*"), outStream.toByteArray());
        val body = MultipartBody.Part.createFormData("file", "image.jpg", fileBody)
        // Create Retrofit
        val retrofit = Retrofit.Builder()
            .baseUrl(SERVICE_URL)
            .build()

        // Create Service
        val service = retrofit.create(EasternAPIService::class.java)
        service.uploadImage(body).enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                Log.d("TAG", "OK")
                if (response.code() == 200) {
                    var bitmap = BitmapFactory.decodeStream(response.body()!!.byteStream())
                    bitmap = bitmap.scale(inputBitmap.width, inputBitmap.height)
                    Log.d("TAG", "${bitmap.width} x ${bitmap.height}")
                    onImageCallback?.let { it(bitmap) }
                } else {
                    Log.d("TAG", "Error code: ${response.code()} -- ${response.message()}")
                    onErrorCallback?.let{ it() }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Log.d("TAG", "Fail")
                onErrorCallback?.let { it() }
            }
        })
    }

}